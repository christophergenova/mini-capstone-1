MINI CAPSTONE:

Theme:
An App that can make the world a happier place.

Goal:
We are currently in a unique situation due to Covid-19. So for your hackathon, create an app that can help individuals, businesses, organization, government, etc. to cope up with the challenges of Covid.

Grading:
20 points: Strength of Purpose
20 points: Complexity
15 points: UI/ UX
15 points: Code Structure
10 points: Commits
10 points: Use of methods/ js concepts that were not discussed in class.
5 points: Audience Impact (Average grade)
5 points: Pushed on time (7:30PM Monday)


To Get an S: Grade must be 88 above.
To Get a P: Grade must be 65 to 80.

BONUS: To get +10: 50% of functionalities should be finished by tomorrow end of class.