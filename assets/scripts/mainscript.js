 

// Realtime date and time

const time = document.getElementById('time')
const currentDate = document.getElementById('date')

const showAmPm = true;

function addZero(n) {
  return (parseInt(n, 10) < 10 ? '0' : '') + n;
}

let timeStamp = "";
let dateStamp = "";

function showDT() {
	let today = new Date(),
		year = today.getFullYear(),
		month = (today.getMonth() + 1 ),
		dateToday = today.getDate(),
		hour = today.getHours(),
		min = today.getMinutes(),
		sec = today.getSeconds();

	const amPm = hour >= 12 ? 'PM' : 'AM';

	hour = hour % 12 || 12;

	time.innerHTML = `${addZero(hour)}<span>:</span>${addZero(min)}<span>:</span>${addZero(
    sec)} ${showAmPm ? amPm : ''}`;

    currentDate.innerHTML = `${addZero(month)}<span>/</span>${addZero(dateToday)}<span>/</span>${year}`;

	setTimeout(showDT, 1000);

	timeStamp = `${addZero(hour)}<span>:</span>${addZero(min)}<span>:</span>${addZero(
    sec)} ${showAmPm ? amPm : ''}`;

	dateStamp = `${addZero(month)}<span>/</span>${addZero(dateToday)}<span>/</span>${year}`;


	return today;
}

showDT();

// Counter
let count = 0;

const counter = document.getElementById('customerCounter')

function updateCounter(){
	counter.textContent = count;
};

updateCounter();

// Declarations
const visitors = []; 

let inputs = document.querySelectorAll('input');

function clearFields(){
	inputs.forEach(input => input.value = "");
};

// FLOW:

// 1. Visitor inputs name, address, and contact number in the form, which will be stored as an object

// 2. When submit button is clicked
const submitBtn = document.getElementById('submitDetails');

submitBtn.addEventListener('click', function(){

	document.getElementById('historyDetails').innerHTML = "";

	document.querySelectorAll('input').innerHTML = "";

	const fullName = submitBtn.parentElement.parentElement.firstElementChild.lastElementChild.value;

	const address = submitBtn.parentElement.parentElement.firstElementChild.nextElementSibling.lastElementChild.value;

	const contactNumber = submitBtn.parentElement.previousElementSibling.lastElementChild.value;

	// 2.1. the created object will be stored in the visitors array
	const customerDetails = {
		name: fullName,
		date: dateStamp,
		time: timeStamp,
		address: address,
		contact: contactNumber
	};

	if( fullName === "" || address === "" || contactNumber === ""){
		alert("Input fields cannot be empty. Please enter complete details.")
		return;
	};

	visitors.push(customerDetails)

	// sort the array in reverse
	visitors.reverse();
	// 2.2. The data will appear inside a table with table headings name, address, contact number, entry date stamps
	visitors.forEach(function(indivDetails){
		const newRow = document.createElement('tr');

		newRow.innerHTML = `<td>${indivDetails.name}</td>
			<td>${indivDetails.date}</td>
			<td>${indivDetails.time}</td>
			<td>${indivDetails.contact}</td>
			<td>${indivDetails.address}</td>
			<td><button class="btn btn-danger positiveBtn">Positive</button></td>
			<td><button class="btn btn-success clearBtn">Recovered</button></td>`
			//Added code TD`

		document.getElementById('historyDetails').appendChild(newRow);
	});

	// 2.3. The counter beside the form will increment by 1
	count++;
	updateCounter();

	// 2.4. Input fields will be cleared
	clearFields();
});

// ---------------------------------
// -------------Added Recovered & Positive Buttons----------



document.addEventListener('click', function(event){
	
	if(event.target.classList.contains('clearBtn')){

		event.target.parentElement.parentElement.classList.add('bg-success');
		event.target.parentElement.parentElement.classList.remove('bg-danger');
	}

	if(event.target.classList.contains('positiveBtn')){

		event.target.parentElement.parentElement.classList.add('bg-danger');
		event.target.parentElement.parentElement.classList.remove('bg-success');
	}

})

// -----------------------------------



// 3. Date and time stamps can be editable (just for presentation purposes)
// 3.1. When the date changes, the visitor counter resets back to zero

// 4. When the admin icon in the corner of the page is clicked a password input field will appear.
const historyBtn = document.getElementById('admin');

const mainScreen = document.getElementById('homescreen');

const pwScreen = mainScreen.nextElementSibling;

historyBtn.addEventListener("click", function(){
	
	mainScreen.classList.add('d-none');

	pwScreen.classList.remove('d-none');

	pwScreen.classList.add('d-flex');

	document.getElementById('passEntry').innerHTML = "";
});

const closeBtn = document.getElementById('closeScreen');

closeScreen.addEventListener("click", function(){
	pwScreen.classList.remove('d-flex');

	pwScreen.classList.add('d-none');

	mainScreen.classList.remove('d-none');
})

//The table can only be accessed by the store's owner or staff so
// 4.1. If the password is wrong, Access Denied will appear below the input field. Otherwise, the history screen will be accessed.
const historyScreen = pwScreen.nextElementSibling;

pwScreen.addEventListener("keypress", function(e){
	const pass = "password";
	const pwValue = document.getElementById('pw').value;
	const p = document.getElementById('pw').nextElementSibling;
	if(e.key === "Enter"){
		if(pwValue !== pass){
			p.textContent = "Access Denied";
		}else if(pwValue === pass){
			pwScreen.classList.remove('d-flex');
			pwScreen.classList.add('d-none');
			historyScreen.classList.remove('d-none');
		}

		clearFields();
	}
});

// 5. The screen containing the table will have a search function for the name
const search = document.getElementById('searchData');
const searchBtn = search.nextElementSibling;
const resultDiv = document.getElementById('searchResults');
const results = resultDiv.lastElementChild;


const print = document.getElementById('downloadPDF');
const exitSearch = document.getElementById('exitSearch');

const backHome = document.getElementById('backToHS');

// 5.1. Typing any key inside the search input field will trigger the search result window
search.addEventListener("keyup", function(){
		resultDiv.classList.remove('d-none');
		const searchInput = search.value;
		const searchText = document.getElementById('searchText');
		searchText.textContent = searchInput;
		exitSearch.classList.remove('d-none');
});

// 5.2. Upon clicking the search button, the table will be filtered by comparing the search field input value to the data in each object inside the visitors array
searchBtn.addEventListener("click", function(){
	results.innerHTML = "";
	const searchInput = search.value;
	const lcInput = searchInput.toLowerCase();
	
	visitors.forEach(function(indivDetails){
		// convert search value to lowercase and compare it to every name value in visitors that are also converted to lowercase
		if(indivDetails.name.toLowerCase().includes(lcInput)){
			const newItem = document.createElement('li');

			newItem.innerHTML = `<span class="returnSameDate"">${indivDetails.date}</span>`
			results.appendChild(newItem);
		}
	});
});

// 5.3. Clicking on the returned date stamp links will hide the rows that do not contain the same date stamp
document.addEventListener('click', function(event){
	if(event.target.classList.contains('returnSameDate')){
		print.classList.remove('d-none');
	}
});

// 5.4. Click exit Search to remove lis, searchDiv, and itself
exitSearch.addEventListener("click", function(){
	resultDiv.classList.add('d-none');
	print.classList.add('d-none');
	exitSearch.classList.add('d-none');
	clearFields();
})


// 6. Back button for the table screen
backHome.addEventListener("click", function(){
	historyScreen.classList.add('d-none');
	mainScreen.classList.remove('d-none');
	resultDiv.classList.add('d-none');
	print.classList.add('d-none');
	exitSearch.classList.add('d-none');
	clearFields();
})